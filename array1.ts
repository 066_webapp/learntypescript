// const names: readonly string[] = [];
const names: string[] = [];
names.push("Dylan"); // no error
names.push("Mook");
// names.push(3); // error
console.log(names);
console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++) {
  console.log(names[i]);
}

for(const index in names) {
  console.log(names[index]);
}

names.forEach(function(name){
  console.log(name);
})

for(const name of names) {
  console.log(name);
}