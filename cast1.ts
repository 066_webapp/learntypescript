let x: unknown = 'hello';
console.log(x);
// console.log(x.length); //error
console.log((x as string).length);

let xx: unknown = 'hello';
console.log((<string>xx).length);

console.log(((x as unknown) as number).toFixed);