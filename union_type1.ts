// function printStatusCode(code: string | number) {
//   console.log(`My status code is ${code.toUpperCase()}.`)
// }
function printStatusCode(code: string | number) {
  if (typeof code === 'string') {
    console.log(`My status code is ${code.toUpperCase()}.`)
  } else {
    console.log(`My status code is ${code}.`)
  }

}
printStatusCode(404);
printStatusCode('ABC');
