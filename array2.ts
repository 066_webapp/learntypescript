const numbers = [1, 2, 3]; // inferred to type number[]
numbers.push(4); // no error
// numbers.push("2"); // error: 
let head: number = numbers[0]; // no error

numbers.forEach(function(value, index) {
  // console.log(value + " " + index);
  console.log(value, index);
})