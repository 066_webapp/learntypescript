let ourTuple: readonly [number, boolean, string];
ourTuple = [5, false, 'Coding God was here'];
// ourTuple = [false, 'Coding God was mistaken', 5]; //error
// ourTuple.push('Something new and wrong'); //error when readonly
// ourTuple.push('2'); //error when readonly
console.log(ourTuple);