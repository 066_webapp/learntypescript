const nameAgeMap: { [index: string]: number } = {};
nameAgeMap.Jack = 25; // no error
// nameAgeMap.Mark = "Fifty"; // Error
nameAgeMap.Mook = 21;
console.log(nameAgeMap);