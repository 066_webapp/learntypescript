const car: { type: string, model: string, year?: number } = {
  type: "Toyota",
  model: "Corolla",
  // year: 2009
};
// const car = {        //js can this
//   type: "Toyota",
//   model: "Corolla",
//   year: 2009
// };
car.type = "Ford"; // no error
// car.type = 2; // error 
console.log(car);