class Person {
  // name: string;

  public constructor(private readonly name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }
}

// const person = new Person();
// person.name = "Jane";
const person = new Person("Jane");
console.log(person);
console.log(person.getName());

interface Shape {
  getArea: () => number;
}

class Rectangle implements Shape {
  public constructor(protected readonly widthh: number, protected readonly heightt: number) { }

  public getArea(): number {
    return this.widthh * this.heightt;
  }

  public toString(): string {
    return `Rectangle[width=${this.widthh}, height=${this.heightt}]`;
  }
}

const rec = new Rectangle(2, 3);
console.log(rec);
console.log(rec.getArea());

console.log(rec.toString());

class Square extends Rectangle {
  public constructor(widthh: number) {
    super(widthh, widthh);
  }

  public override toString(): string {
    return `Square[width=${this.widthh}]`;
  }
}

const square = new Square(2);
console.log(square);
console.log(square.getArea());

console.log(square.toString());

abstract class Polygon {
  public abstract getArea(): number;

  public toString(): string {
    return `Polygon[area=${this.getArea()}]`;
  }
}

class Rectangle2 extends Polygon {
  public constructor(protected readonly widthh: number, protected readonly heightt: number) {
    super();
  }

  public getArea(): number {
    return this.widthh * this.heightt;
  }
}

const rectanglee = new Rectangle2(3,4);
console.log(rectanglee);
console.log(rectanglee.getArea());